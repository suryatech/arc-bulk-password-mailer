/**
 * Author:    Surya Pappoppula - 2011C6PS480G
 * Email:     suryasongs@gmail.com
 * Created:   27.05.2014
 * Summary:   Send the ERP login passwords to all students in one go!
 **/

var email = require('emailjs'),
    configs = require('./config.js').configs,
    reader = require('line-by-line'),
    lr = new reader(configs.fileName),
    student,
    password,
    emailHandle,
    cols,
    data,
    mailServer = email.server.connect({
        user: configs.username,
        password: configs.password,
        host: configs.smtpServer,
        ssl: true
    }),
    copy = email.server.connect({
        user: configs.username,
        password: configs.password,
        host: configs.smtpServer,
        ssl: true
    }),
    mailMessage = {
        from: "ARC Office<" + configs.username + ">",   
        subject: "ERP ARC Module Login Information"
    },
    errorMessage = {
        from: "ARC Office<" + configs.username + ">",
        subject: "Error Logs - Password Mailer",
        to: "<suryasongs@gmail.com>"
    };

lr.on('error', function(err) {
    // 'err' contains error object
    console.log(err);
});

lr.on('line', function(line) {
    // 'line' contains the current line without the trailing newline character.
    cols = line.split(",");
    student = cols[0];
    password = cols[1];

    emailHandle = '<f' + student.substr(0, 4) + '' + student.substr(8, 3) + '@goa.bits-pilani.ac.in>';
    mailMessage.to = emailHandle;

    // Email Template
    mailMessage.text = "Dear Student,\n\nYour login information for the ERP ARC Module is,";
    mailMessage.text += "\n\nUsername: " + student + "\nPassword: " + password;
    mailMessage.text += "\n\nTo access the module, visit https://erp.bits-pilani.ac.in:4431/psp/hcsprod/?cmd=logout";

    mailMessage.text += "\n\n\nAutomated mailer developed by Surya P, 2011C6PS480G";
    console.log(student + ' : ' + password);

    // Uncomment the following lines when everything is alright. 
    /*mailServer.send(mailMessage, function(err, message) {
        if (err) {
            console.log(err);
            console.log("Error sending mail to " + student);
            console.log('\n');

            errorMessage.text = JSON.stringify(err);
            copy.send(errorMessage, function(error, message) {
                if (error) {
                    console.log("Error sending error logs to developer.");
                    console.log(error);
                }
            });
        }
    });*/
});

lr.on('end', function() {
    // All lines are read, file is closed now.
    console.log("Finished processing all records");
});
